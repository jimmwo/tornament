<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <script src="/js/app.js" defer></script>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
    <div class="container-xl">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item {{ (request()->is('/')) ? 'active' : '' }}">
                            <a class="nav-link" href="{!! route('index'); !!}">Home</a>
                        </li>
                        <li class="nav-item {{ (request()->is('grid')) ? 'active' : '' }}">
                            <a class="nav-link" href="{!! route('grid'); !!}">Grid</a>
                        </li>
                        <li class="nav-item {{ (request()->is('grid-play-off')) ? 'active' : '' }}">
                            <a class="nav-link" href="{!! route('playOffGrid'); !!}">Play-off Grid</a>
                        </li>
                        <li class="nav-item {{ (request()->is('results')) ? 'active' : '' }}">
                            <a class="nav-link" href="{!! route('results'); !!}">Result</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        @yield('content')
    </div>
</div>


@stack('js')
</body>
</html>
