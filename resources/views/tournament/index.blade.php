@extends('layouts.app')

@section('title', 'Main Page Tournament')

@section('content')
    <div class="row">
        <div class="col">
            <header class="masthead bg-primary text-white text-center">
                <div class="container d-flex align-items-center flex-column">
                    <!-- Masthead Avatar Image-->
                    <img class="masthead-avatar mb-5" src="/images/avataaars.svg" alt="" />
                    <!-- Masthead Heading-->
                    <h1 class="masthead-heading text-uppercase mb-0"><a href="{!! url('/grid'); !!}">Start Tournament</a></h1>
                    <!-- Icon Divider-->
                    <div class="divider-custom divider-light">
                        <div class="divider-custom-line"></div>
                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                        <div class="divider-custom-line"></div>
                    </div>
                    <!-- Masthead Subheading-->
                    <p class="masthead-subheading font-weight-light mb-0">Welcome to the tournament generator</p>
                </div>
            </header>
        </div>
    </div>
@endsection
