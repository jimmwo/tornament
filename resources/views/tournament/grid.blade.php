@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
    <div class="row> mb-5 mt-1">
        <form method="POST" action="/generate">
            @csrf
            <button type="submit" class="btn btn-primary">Generate grid</button>
        </form>
    </div>
    <div class="row">{{$table->renderTable()}}</div>
@endsection
