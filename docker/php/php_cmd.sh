#!/usr/bin/env bash

if [ $DEBUG_MODE -eq 1 ]; then
NGINX_PROD=0
echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/xdebug.ini
else
NGINX_PROD=1
fi
echo "upload_max_filesize=2M" >> /usr/local/etc/php/conf.d/upload-max-size.ini
echo "post_max_size=2M" >> /usr/local/etc/php/conf.d/post_max_size.ini
echo "memory_limit=128M" >> /usr/local/etc/php/conf.d/memory_limit.ini
sudo chgrp -R www-data storage bootstrap/cache storage
sudo chmod -R ug+rwx storage bootstrap/cache

composer install --prefer-dist  --no-scripts --no-interaction
echo "Running migrations..."

php artisan migrate --seed
php artisan cache:clear
php artisan config:cache
php artisan route:cache
php artisan view:cache
php artisan ui bootstrap
npm i
npm run production
rm -rf ./tmp/cache/*


echo "Starting php-fpm"
php-fpm
