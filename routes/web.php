<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TournamentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TournamentController::class, 'index'])->name('index');
Route::get('/grid', [TournamentController::class, 'grid'])->name('grid');
Route::get('/grid-play-off', [TournamentController::class, 'playOffGrid'])->name('playOffGrid');
Route::get('/results', [TournamentController::class, 'results'])->name('results');
Route::post('/generate', [TournamentController::class, 'generateGames'])->name('generateGames');
Route::post('/generate-play-off', [TournamentController::class, 'generatePlayOffGames'])->name('generatePlayOffGames');
