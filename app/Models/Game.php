<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Game extends Model
{
    use HasFactory;

    const POINTS_FOR_WINS = 3;
	const POINTS_FOR_DRAW = 1;
	const POINTS_FOR_LOSS = 0;

	/**
	 * @return BelongsTo
	 */
	public function firstTeam(): BelongsTo
	{
		return $this->belongsTo('App\Models\Team', 'first_team_id', 'id');
	}

	/**
	 * @return BelongsTo
	 */
	public function secondTeam(): BelongsTo
	{
		return $this->belongsTo('App\Models\Team', 'second_team_id', 'id');
	}

	/**
	 * @return BelongsTo
	 */
	public function division(): BelongsTo
	{
		return $this->belongsTo('App\Models\Team', 'division_id', 'id');
	}

	public static function getScorePerGame($firstTeamGoals, $secondTeamGoals)
	{
		if ($firstTeamGoals > $secondTeamGoals) {
			return self::POINTS_FOR_WINS;
		} else if ($firstTeamGoals < $secondTeamGoals) {
			return self::POINTS_FOR_LOSS;
		} else {
			return self::POINTS_FOR_DRAW;
		}
	}
}
