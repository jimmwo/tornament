<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Support\Collection;

class Team extends Model
{
    use HasFactory;

	/**
	 * @return HasOneThrough
	 */
	public function division(): HasOneThrough
	{
		return $this->hasOneThrough('App\Models\Division', 'App\Models\DivisionTeam', 'team_id', 'id', 'id', 'division_id');
	}

	/**
	 * @return HasMany
	 */
	public function homeGames(): HasMany {
		return $this->hasMany('App\Models\Game', 'first_team_id');
	}

	/**
	 * @return HasMany
	 */
	public function guestGames(): HasMany {
		return $this->hasMany('App\Models\Game', 'second_team_id');
	}

	/**
	 * @return Collection
	 */
	public function games(): Collection {
		return $this->homeGames->merge($this->guestGames);
	}
}
