<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Division extends Model
{
    use HasFactory;

	/**
	 * @return HasManyThrough
	 */
    public function teams(): HasManyThrough
    {
	    return $this->hasManyThrough('App\Models\Team', 'App\Models\DivisionTeam', 'division_id', 'id', 'id', 'team_id');
    }

	/**
	 * @return HasMany
	 */
	public function games(): HasMany
	{
		return $this->hasMany('App\Models\Game');
	}
}
