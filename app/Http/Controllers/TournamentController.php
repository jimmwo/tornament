<?php

namespace App\Http\Controllers;

use App\Services\Generator\DivisionsGenerator;
use App\Services\Generator\GridGamesGenerator;
use App\Services\Generator\PlayOffGamesGenerator;
use App\Services\Table\MainGridStrategy;
use App\Services\Table\PlayOffGridStrategy;
use App\Services\Table\ResultsGridStrategy;
use App\Services\Table\Table;

class TournamentController extends Controller
{

	/**
	 * Main page of the tournament.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('tournament.index');
	}

    /**
     * Display a grid of the tournament.
     *
     * @return \Illuminate\Http\Response
     */
    public function grid()
    {
	    $table = new Table(new MainGridStrategy());

	    return view('tournament.grid', ['table' => $table]);
    }

    /**
     * Display a grid of the tournament play-off.
     *
     * @return \Illuminate\Http\Response
     */
    public function playOffGrid()
    {
	    $table = new Table(new PlayOffGridStrategy());

	    return view('tournament.grid-play-off', ['table' => $table]);
    }

	/**
	 * Generate the main tournament games
	 *
	 * @param DivisionsGenerator $divisionsGenerator
	 * @param GridGamesGenerator $gamesGenerator
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function generateGames(DivisionsGenerator $divisionsGenerator, GridGamesGenerator $gamesGenerator)
    {
	    $divisionsGenerator->createGenerator()->generate();
	    $gamesGenerator->createGenerator()->generate();

	    return redirect()->route('grid');
    }

	/**
	 * Generate play-off tournament games
	 *
	 * @param PlayOffGamesGenerator $generator
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function generatePlayOffGames(PlayOffGamesGenerator $generator)
	{
		$generator->createGenerator()->generate();

		return redirect()->route('playOffGrid');
	}

    /**
     * Display the results table.
     *
     * @return \Illuminate\Http\Response
     */
    public function results()
    {
	    $table = new Table(new ResultsGridStrategy());

	    return view('tournament.results', ['table' => $table]);
    }
}
