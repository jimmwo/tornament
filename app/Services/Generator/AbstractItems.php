<?php

namespace App\Services\Generator;

abstract class AbstractItems implements Items
{
	private const MAX_GOAL_IN_GAME = 4;

	/**
	 * Generator
	 */
	abstract public function generate(): void;

	/**
	 * @param int|null $exclude
	 * @return int
	 * @throws \Exception
	 */
	protected function getNumberOfGoals(?int $exclude = null): int
	{
		$number = random_int(0, self::MAX_GOAL_IN_GAME);
		if (!is_null($exclude) && $number === $exclude) {
			return $this->getNumberOfGoals($exclude);
		}

		return $number;
	}
}
