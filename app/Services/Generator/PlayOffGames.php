<?php


namespace App\Services\Generator;

use App\Models\Division;
use App\Models\Game;

class PlayOffGames extends AbstractItems
{
	/**
	 * @var Division[]|\Illuminate\Database\Eloquent\Collection
	 */
	private $divisions;

	const NUMBER_OF_TEAMS_IN_PLAY_OFF = 4;

	const BRANCH_PLAY_OFF_FIRST = 1;
	const BRANCH_PLAY_OFF_SECOND = 2;
	const BRANCH_FINAL = 3;

	public function __construct()
	{
		$this->divisions = Division::all();
	}

	/**
	 * Generate play-off games
	 */
	public function generate(): void
	{
		Game::whereNotNull('play_off_stage')->delete();

		$result = [];
		foreach ($this->divisions as $division) {
			$divisionGames = $this->getDivisionGames($division->games->toArray());
			$gamesByScore = $this->getGamesByScore($divisionGames);
			$result[$division->id] = $this->getWinners($gamesByScore);
		}

		$this->createGrid(...$result);
	}

	/**
	 * @param array $games
	 * @return array
	 */
	private function getDivisionGames(array $games): array
	{
		$result = [];
		foreach ($games as $game) {
			$result[$game['first_team_id']][$game['second_team_id']] = [
				'teamId' => $game['first_team_id'],
				'goalsScored' => $game['number_of_goals_first_team'],
				'goalsMissed' => $game['number_of_goals_second_team'],
			];
			$result[$game['second_team_id']][$game['first_team_id']] = [
				'teamId' => $game['second_team_id'],
				'goalsScored' => $game['number_of_goals_second_team'],
				'goalsMissed' => $game['number_of_goals_first_team'],
			];
		}

		return $result;
	}

	/**
	 * @param array $games
	 * @return array
	 */
	private function getGamesByScore(array $games): array
	{
		$result = [];
		foreach ($games as $teamId => $teamGames) {
			$score = 0;
			foreach ($teamGames as $opponentTeamId => $game) {
				$score += Game::getScorePerGame($game['goalsScored'], $game['goalsMissed']);
			}

			$result[$teamId] = $score;
			arsort($result);
		}

		return $result;
	}

	/**
	 * @param array $firstDivision
	 * @param array $secondDivision
	 * @param int $stage
	 * @param int|null $branch
	 * @return array
	 * @throws \Exception
	 */
	private function createGrid(array $firstDivision, array $secondDivision, int $stage = 1, ?int $branch = null): array
	{
		$gridGames = $this->generateGridGames($firstDivision, $secondDivision, $stage, $branch);
		$resultGames = $this->getGridResults($gridGames, ++$stage);

		if (!$this->isLastStage($resultGames)) {
			return $this->generateGridFinalGames($resultGames, $stage);
		}

		return $resultGames;
	}

	/**
	 * @param array $firstDivision
	 * @param array $secondDivision
	 * @param int $stage
	 * @param int|null $branch
	 * @return array
	 * @throws \Exception
	 */
	private function generateGridGames(array $firstDivision, array $secondDivision, int $stage, ?int $branch): array
	{
		$result = $insertData = [];
		$countTeams = count($firstDivision);
		foreach (range(1, $countTeams) as $step) {
			$currentBranch = $this->getBranch($countTeams, $step, $branch);
			$insertData[] = $result[$currentBranch][] = $this->getInsertData($firstDivision, $secondDivision, $stage, $currentBranch);
		}

		Game::insert($insertData);

		return $result;
	}

	/**
	 * @param array $gridGames
	 * @param int $stage
	 * @return array
	 */
	private function getGridResults(array $gridGames, int $stage): array
	{
		$resultGames = [];
		foreach ($gridGames as $branchId => $games) {
			$divisionGames = $this->getDivisionGames($games);
			$gamesByScore = $this->getGamesByScore($divisionGames);
			$resultGames[$branchId]['winners'] = $this->getWinners($gamesByScore, self::NUMBER_OF_TEAMS_IN_PLAY_OFF / $stage);
			$resultGames[$branchId]['losers'] = $this->getLosers($gamesByScore, self::NUMBER_OF_TEAMS_IN_PLAY_OFF / $stage);
		}

		return $resultGames;
	}

	/**
	 * @param array $firstBranchResults
	 * @param array $secondBranchResults
	 * @param int $stage
	 * @param int|null $branch
	 * @return array
	 * @throws \Exception
	 */
	private function getInsertData(array &$firstBranchResults, array &$secondBranchResults, int $stage, ?int $branch = null): array
	{
		$firstTeamId = $this->getFirstTeamId($firstBranchResults);
		$secondTeamId = $this->getLastTeamId($secondBranchResults);

		return $this->getGameResult($firstTeamId, $secondTeamId, $stage, $branch);
	}

	/**
	 * @param int $firstTeamId
	 * @param int $secondTeamId
	 * @param int $stage
	 * @param int|null $branch
	 * @return array
	 * @throws \Exception
	 */
	private function getGameResult(int $firstTeamId, int $secondTeamId, int $stage, ?int $branch = null): array
	{
		$numberOfGoalsFirstTeam = self::getNumberOfGoals();
		return [
			'first_team_id' => $firstTeamId,
			'second_team_id' => $secondTeamId,
			'number_of_goals_first_team' => $numberOfGoalsFirstTeam,
			'number_of_goals_second_team' => self::getNumberOfGoals($numberOfGoalsFirstTeam),
			'play_off_stage' => $stage,
			'play_off_branch' => $branch
		];
	}

	/**
	 * @param array $resultGames
	 * @param int $stage
	 * @return array
	 * @throws \Exception
	 */
	private function generateGridFinalGames(array $resultGames, int $stage): array
	{
		$firstBranchResults = $this->createGrid([$resultGames[self::BRANCH_PLAY_OFF_FIRST]['winners'][0]], [$resultGames[self::BRANCH_PLAY_OFF_FIRST]['winners'][1]], $stage, self::BRANCH_PLAY_OFF_FIRST);
		$secondBranchResults = $this->createGrid([$resultGames[self::BRANCH_PLAY_OFF_SECOND]['winners'][0]], [$resultGames[self::BRANCH_PLAY_OFF_SECOND]['winners'][1]], $stage, self::BRANCH_PLAY_OFF_SECOND);

		$insertData = [];
		$insertData[] = $this->getInsertData($firstBranchResults[self::BRANCH_PLAY_OFF_FIRST]['losers'], $secondBranchResults[self::BRANCH_PLAY_OFF_SECOND]['losers'], ++$stage, self::BRANCH_FINAL);
		$insertData[] = $this->getInsertData($firstBranchResults[self::BRANCH_PLAY_OFF_FIRST]['winners'], $secondBranchResults[self::BRANCH_PLAY_OFF_SECOND]['winners'], ++$stage, self::BRANCH_FINAL);

		Game::insert($insertData);

		return $insertData;
	}

	/**
	 * @param array $gamesByScoree
	 * @param int $numbersOfWinsTeam
	 * @return array
	 */
	private function getWinners(array $gamesByScoree, int $numbersOfWinsTeam = self::NUMBER_OF_TEAMS_IN_PLAY_OFF): array
	{
		return array_slice(array_keys($gamesByScoree), 0, $numbersOfWinsTeam, true);

	}

	/**
	 * @param array $gamesByScore
	 * @param int $numbersOfWinsTeam
	 * @return array
	 */
	private function getLosers(array $gamesByScore, int $numbersOfWinsTeam): array
	{
		return array_slice(array_keys($gamesByScore), $numbersOfWinsTeam, null, true);

	}

	/**
	 * @param array $resultGames
	 * @return bool
	 */
	private function isLastStage(array $resultGames): bool
	{
		return count($resultGames) === 1;
	}

	/**
	 * @param array $results
	 * @return int
	 */
	private function getFirstTeamId(array &$results): int
	{
		return array_shift($results);
	}

	/**
	 * @param array $results
	 * @return int
	 */
	private function getLastTeamId(array &$results): int
	{
		return array_pop($results);
	}

	/**
	 * @param int $countTeams
	 * @param int $step
	 * @param int|null $branch
	 * @return int
	 */
	private function getBranch(int $countTeams, int $step, ?int $branch): int
	{
		if (is_null($branch)) {
			return $step > $countTeams / 2 ? self::BRANCH_PLAY_OFF_SECOND : self::BRANCH_PLAY_OFF_FIRST;
		} else {
			return $branch;
		}
	}
}
