<?php

namespace App\Services\Generator;

class PlayOffGamesGenerator implements GeneratorFactory
{
	public function createGenerator(): Items
	{
		return new PlayOffGames();
	}
}
