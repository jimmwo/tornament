<?php

namespace App\Services\Generator;

interface Items
{
	public function generate(): void ;
}
