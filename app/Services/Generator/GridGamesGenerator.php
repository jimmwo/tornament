<?php

namespace App\Services\Generator;

class GridGamesGenerator implements GeneratorFactory
{
	public function createGenerator(): Items
	{
		return new GridGames();
	}
}
