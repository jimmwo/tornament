<?php


namespace App\Services\Generator;

class DivisionsGenerator implements GeneratorFactory
{
	public function createGenerator(): Items
	{
		return new Divisions();
	}
}
