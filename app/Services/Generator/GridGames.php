<?php

namespace App\Services\Generator;

use App\Models\Division;
use App\Models\Game;

class GridGames extends AbstractItems
{
	/**
	 * @var Division[]|\Illuminate\Database\Eloquent\Collection
	 */
	private $divisions;

	public function __construct()
	{
		$this->divisions = Division::all();
	}

	/**
	 * Generated main grid games
	 */
	public function generate(): void
	{
		Game::truncate();
		foreach ($this->divisions as $division) {
			$games = $this->prepareToInsert($division);

			Game::insert($games);
		}
	}

	/**
	 * @param Division $division
	 * @return array
	 * @throws \Exception
	 */
	protected function prepareToInsert(Division $division): array
	{
		$games = [];
		foreach ($division->teams as $firstKey => $firstTeam) {
			foreach ($division->teams as $secondKey => $secondTeam) {
				if ($firstKey >= $secondKey) continue;

				$games[] = [
					'division_id' => $division->id,
					'first_team_id' => $firstTeam->id,
					'second_team_id' => $secondTeam->id,
					'number_of_goals_first_team' => $this->getNumberOfGoals(),
					'number_of_goals_second_team' => $this->getNumberOfGoals(),
				];
			}
		}

		return $games;
	}
}
