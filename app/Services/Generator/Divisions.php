<?php


namespace App\Services\Generator;

use App\Models\Division;
use App\Models\DivisionTeam;
use App\Models\Team;


class Divisions extends AbstractItems
{
	/**
	 * @var Division[]|\Illuminate\Database\Eloquent\Collection
	 */
	private $divisions;

	/**
	 * @var Team[]|\Illuminate\Database\Eloquent\Collection
	 */
	private $teams;

	public function __construct()
	{
		$this->divisions = Division::all();
		$this->teams = Team::all()->shuffle();
	}

	/**
	 * Generated division teams
	 */
	public function generate(): void
	{
		DivisionTeam::truncate();

		$divisionTeams = $this->prepareToInsert();

		DivisionTeam::insert($divisionTeams);
	}

	/**
	 * @return array
	 */
	private function prepareToInsert(): array
	{
		$numberOfDivisions = $this->divisions->count();
		$divisionTeams = [];
		foreach ($this->teams as $key => $team) {
			$teamIndex = $key % $numberOfDivisions;
			$divisionTeams[] = [
				'division_id' => $this->divisions[$teamIndex]->id,
				'team_id' => $team->id
			];
		}

		return $divisionTeams;
	}
}
