<?php

namespace App\Services\Generator;

interface GeneratorFactory
{
	public function createGenerator(): Items;
}
