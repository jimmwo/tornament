<?php

namespace App\Services\Table;

interface TableStrategy
{
	const QUARTER_FINAL_STEP = 1;
	const SEMI_FINAL_STEP = 2;
	const THIRD_PLACE_STEP = 3;
	const FINAL_STEP = 4;

	public function render(): string;
}
