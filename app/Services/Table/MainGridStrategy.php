<?php

namespace App\Services\Table;

use App\Models\Division;
use App\Models\Game;
use Illuminate\Database\Eloquent\Collection;

class MainGridStrategy implements TableStrategy
{
	/**
	 * @var Division[]|Collection
	 */
	private $divisions;

	const OWN_CELL_TAG = 'gray';

	public function __construct()
	{
		$this->divisions = Division::all();
	}

	/**
	 * Render a table
	 *
	 * @return string
	 */
	public function render(): string
	{
		$content = '';
		$divisions = Division::all();
		foreach ($divisions as $division) {
			$games = $this->getGames($division);
			if ($games) {
				$content .= $this->renderOneDivision($games, $division->name);
			}
		}

		return $content;
	}

	/**
	 * @param Division $division
	 * @return array
	 */
	private function getGames(Division $division): array
	{
		$result = [];
		foreach ($division->games as $key => $game) {
			$result[$game->first_team_id]['teamName'] = $game->firstTeam->name;
			$result[$game->first_team_id]['teamResults'][$game->second_team_id] = [
				$game->number_of_goals_first_team,
				$game->number_of_goals_second_team
			];

			$result[$game->second_team_id]['teamName'] = $game->secondTeam->name;
			$result[$game->second_team_id]['teamResults'][$game->first_team_id] = [
				$game->number_of_goals_second_team,
				$game->number_of_goals_first_team
			];
		}
		ksort($result);

		return $result;
	}

	/**
	 * @param array $games
	 * @return array
	 */
	private function prepareResults(array $games): array
	{
		$result = [];
		foreach ($games as $firstTeamId => $firstTeamGames) {
			$score = 0;
			$result[$firstTeamGames['teamName']]['teamResults'][$firstTeamId] = self::OWN_CELL_TAG;
			$result[$firstTeamGames['teamName']]['teamId'] = $firstTeamId;
			foreach ($firstTeamGames['teamResults'] as $secondTeamId => $results) {
				$result[$firstTeamGames['teamName']]['teamResults'][$secondTeamId] = $results[0] . ':' . $results[1];

				$score += Game::getScorePerGame($results[0], $results[1]);
			}

			$result[$firstTeamGames['teamName']]['score'] = $score;
			ksort($result[$firstTeamGames['teamName']]['teamResults']);
		}

		return $result;
	}

	/**
	 * @param array $games
	 * @param string $divisionName
	 * @return string
	 */
	private function renderOneDivision(array $games, string $divisionName): string
	{
		$results = $this->prepareResults($games);

		return "<div class='col'>
			<table class='table table-bordered'>
	            <thead>
	            <tr class='text-center'>
	                <th scope='col' colspan='10'>Division $divisionName</th>
	            </tr>
	            <tr>
	                <th scope='col'>Teams</th>
	                {$this->getTableHead($results)}
	                <th scope='col'>Score</th>
	            </tr>
	            </thead>
	            <tbody>
	                {$this->getTableRows($results)}
	            </tbody>
	        </table>
	    </div>";
	}

	/**
	 * @param array $results
	 * @return string
	 */
	private function getTableHead(array $results): string
	{
		$head = '';
		foreach (array_keys($results) as $team) {
			$head .= "<th scope='col'>$team</th>";
		}

		return $head;
	}

	/**
	 * @param array $results
	 * @return string
	 */
	private function getTableRows(array $results): string
	{
		$rows = '';
		foreach ($results as $team => $row) {
			$rows .= "<tr><th scope='row'>$team</th>";
			foreach ($row['teamResults'] as $res) {
				$rows .= "<td class='" . ($res === self::OWN_CELL_TAG ? 'bg-dark' : '') . "'>" . ($res !== self::OWN_CELL_TAG ? $res : '') . "</td>";
			}
			$rows .= "<td>{$row['score']}</td>";
		}

		return $rows;
	}
}
