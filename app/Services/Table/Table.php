<?php

namespace App\Services\Table;

class Table
{
	/**
	 * @var TableStrategy
	 */
	private $strategy;

	public function __construct(TableStrategy $strategy)
	{
		$this->strategy = $strategy;
	}

	/**
	 * @param TableStrategy $strategy
	 */
	public function setStrategy(TableStrategy $strategy)
	{
		$this->strategy = $strategy;
	}

	/**
	 * Drawing a table
	 */
	public function renderTable(): void
	{
		echo $this->strategy->render();
	}
}
