<?php

namespace App\Services\Table;

use App\Models\Game;
use Illuminate\Database\Eloquent\Collection;

class ResultsGridStrategy implements TableStrategy
{
	/**
	 * @var Game[]|Collection
	 */
	private $games;

	const COUNT_TEAMS_IN_GAME = 2;

	public function __construct()
	{
		$this->games = Game::whereNull('division_id')
			->whereIn('play_off_stage', [self::THIRD_PLACE_STEP, self::FINAL_STEP])
			->orderBy('play_off_stage', 'desc')
			->get();
	}

	/**
	 * Render a table
	 *
	 * @return string
	 */
	public function render(): string
	{
		$content = "<div class='col'>
            <table class='table table-bordered'>
                <thead>
                    <tr class='text-center'>
                        <th scope='col' colspan='10'>Result</th>
                    </tr>
                    <tr>
                        <th scope='col'>Place</th>
                        <th scope='col'>Team</th>
                    </tr>
                </thead>
                <tbody>";

		$content .= $this->getContentByGames();

		$content .= "</tbody></table></div>";


		return $content;
	}

	/**
	 * @return string
	 */
	private function getContentByGames(): string
	{
		$place = 1;
		$content = '';
		foreach ($this->games as $game) {
			$content .= $this->getContentByOneGame($game, $place);
			$place += self::COUNT_TEAMS_IN_GAME;
		}

		return $content;
	}

	/**
	 * @param Game $game
	 * @param int $place
	 * @return string
	 */
	private function getContentByOneGame(Game $game, int $place): string
	{
		list($winner, $loser) = $this->getGameTeams($game);

		$content = '';
		$content .= "<tr>
                <th scope='row'>{$place}</th>
                <td>Team {$winner->name}</td>
            </tr>";
		$place++;
		$content .= "<tr>
                <th scope='row'>{$place}</th>
                <td>Team {$loser->name}</td>
            </tr>";

		return $content;
	}

	/**
	 * @param Game $game
	 * @return array
	 */
	private function getGameTeams(Game $game): array
	{
		if ($game->number_of_goals_first_team > $game->number_of_goals_second_team) {
			$winner = $game->firstTeam;
			$loser = $game->secondTeam;
		} else {
			$winner = $game->secondTeam;
			$loser = $game->firstTeam;
		}

		return [$winner, $loser];
	}
}
