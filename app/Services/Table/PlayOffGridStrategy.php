<?php

namespace App\Services\Table;

use App\Models\Game;
use Illuminate\Database\Eloquent\Collection;

class PlayOffGridStrategy implements TableStrategy
{
	/**
	 * @var Game[]|Collection
	 */
	private $games;

	const OWN_CELL_TAG = 'gray';

	const QUARTER_FINAL_TEXT = 'Quarter-final';
	const SEMI_FINAL_TEXT = 'Semi-final';
	const THIRD_PLACE_TEXT = 'Match of third place';
	const FINAL_TEXT = 'Final';

	public function __construct()
	{
		$this->games = Game::whereNull('division_id')->get()->groupBy(['play_off_stage']);
	}

	/**
	 * @return array
	 */
	private function mapStages(): array
	{
		return [
			self::QUARTER_FINAL_STEP => self::QUARTER_FINAL_TEXT,
			self::SEMI_FINAL_STEP => self::SEMI_FINAL_TEXT,
			self::THIRD_PLACE_STEP => self::THIRD_PLACE_TEXT,
			self::FINAL_STEP => self::FINAL_TEXT,
		];
	}

	/**
	 * @param int $stageId
	 * @return string
	 */
	private function getStageName(int $stageId): string
	{
		return $this->mapStages()[$stageId];
	}

	/**
	 * Render a table
	 *
	 * @return string
	 */
	public function render(): string
	{
		$content = '';
		foreach ($this->games as $stageId => $games) {
			$content .= "<div class='col-sm result_col'><div class='head_block'>{$this->getStageName($stageId)}</div>";
			foreach ($games as $key => $game) {
				$content .= "<div class='block_result {$this->getClassForDiv($stageId)}'>
					{$game->firstTeam->name}<hr>{$game->secondTeam->name}
					<span class='game_score {$this->getClassForSpan($stageId)}'>$game->number_of_goals_first_team : $game->number_of_goals_second_team</span>
				</div>";
			}
			$content .= "</div>";
		}

		return $content;
	}

	/**
	 * @param int $stageId
	 * @return string
	 */
	private function getClassForDiv(int $stageId): string
	{
		if ($stageId === self::SEMI_FINAL_STEP) {
			return 'second';
		} else if ($stageId === self::THIRD_PLACE_STEP || $stageId === self::FINAL_STEP) {
			return 'third';
		} else {
			return '';
		}
	}

	/**
	 * @param int $stageId
	 * @return string
	 */
	private function getClassForSpan(int $stageId): string
	{
		if ($stageId === self::SEMI_FINAL_STEP) {
			return 'semi-final';
		} else if ($stageId === self::THIRD_PLACE_STEP || $stageId === self::FINAL_STEP) {
			return 'final';
		} else {
			return '';
		}
	}
}
