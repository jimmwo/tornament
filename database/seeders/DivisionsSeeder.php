<?php

namespace Database\Seeders;

use App\Models\Division;
use Illuminate\Database\Seeder;

class DivisionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $divisions = range('A', 'B');

	    $insertData = [];
	    foreach ($divisions as $division) {
		    $insertData[] = [
			    'name' => $division,
		    ];
	    }

	    Division::insert($insertData);
    }
}
