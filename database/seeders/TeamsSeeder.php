<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $teams = range('A', 'P');

	    $insertData = [];
	    foreach ($teams as $team) {
		    $insertData[] = [
			    'name' => $team,
		    ];
	    }

	    Team::insert($insertData);
    }
}
