<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
	        $table->id();
	        $table->bigInteger('first_team_id')->unsigned();
	        $table->bigInteger('second_team_id')->unsigned();
	        $table->bigInteger('division_id')->unsigned()->nullable();
	        $table->tinyInteger('play_off_stage')->unsigned()->nullable();
	        $table->tinyInteger('play_off_branch')->unsigned()->nullable();
	        $table->tinyInteger('number_of_goals_first_team')->unsigned();
	        $table->tinyInteger('number_of_goals_second_team')->unsigned();
	        $table->timestamp('created_at')->useCurrent();

	        $table->foreign('division_id')
		        ->references('id')
		        ->on('divisions')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');

	        $table->foreign('first_team_id')
		        ->references('id')
		        ->on('teams')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');

	        $table->foreign('second_team_id')
		        ->references('id')
		        ->on('teams')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('games', function (Blueprint $table) {
		    $table->dropForeign(["division_id"]);
		    $table->dropForeign(["first_team_id"]);
		    $table->dropForeign(["second_team_id"]);
	    });

        Schema::dropIfExists('games');
    }
}
