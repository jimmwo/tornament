<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDivisionTeams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('division_teams', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('division_id')->unsigned();
	        $table->bigInteger('team_id')->unsigned();

	        $table->foreign('division_id')
		        ->references('id')
		        ->on('divisions')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');

	        $table->foreign('team_id')
		        ->references('id')
		        ->on('teams')
		        ->onDelete('cascade')
		        ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		    Schema::table('division_teams', function (Blueprint $table) {
			    $table->dropForeign(["division_id"]);
			    $table->dropForeign(["team_id"]);
		    });

        Schema::dropIfExists('division_teams');
    }
}
